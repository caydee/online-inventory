<!doctype HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Select Cities</title>
<script type="text/javascript">
//
// initialise shortcuts on page load
var selectObj1, selectObj2; // global
 function init()
  { selectObj1=document.getElementById("select_1");
    selectObj2=document.getElementById("select_2");
    selectObj2.disabled=true; 
  }
// ========== end init() ============== 
// second select box options list
var A= new Array()
A["NSW"]=["Select here ...","Sydney","Newcastle","Dubbo"];
A["Victoria"]=["Select here ...","Melbourne","Portland","Geelong"];
A["Queensland"]=["Select here ...","Brisbane","Gladstone","Cairns"];
//
// global variables
var saveObj=null, indx=null, targetObj=null, selectNo=null;   
// 
 function process(obj,sNumb)
  { // disable unused select boxes 
    if(sNumb==2)
     { selectObj2.selectedIndex="0";
       selectObj2.disabled=true;
     }
   //
   // store selected index   
    indx=obj.options.selectedIndex;
    if(indx==0){ return; }               // invalid selection 
   // ---------
   // save passed parameters for use after timeout deleay   
    saveObj=obj, selectNo=sNumb;     
  // put object items list into next select box after clearing
     targetObj=document.getElementById("select_"+selectNo)
     targetObj.disabled=false;
   // clear any existing options note that this starts from end of list, not beginning  
     for(var i=targetObj.options.length-1;i>-1;i--)
          { targetObj.options[i]=null;  }   
  // build in short delay here to avoid error in Opera browser
      setTimeout("finishOff()",50)
    }
// ----------- 50ms delay here --------   
// called from timeout in function process()
   function finishOff()
    {  var obj=saveObj;    // from global
      // fill selectObj options  
       var i, thisItem=0;                         
      // build options list
      var targArray = A[obj.options[indx].value];   
      //
       for(i=0;i<targArray.length;i++)
         { thisItem=targArray[i];      
          // syntax is new Option("text", "value", isDefaultSelectedFlag, isSelectedFlag)
                    
           targetObj.options[i]=new Option(thisItem,thisItem,false,false);    
         } 
     obj.blur(); 
  }
// ============ end process() ===================
// fires on selecting in second select box
 function finish()
  { if(selectObj2.selectedIndex==0){return; }    // invalid selection    
    alert("You have selected ["+selectObj1.value+"]  ["+selectObj2.value+"]")
  }
// ------------  
//
window.onload=init;
// 
</script>
<style type="text/css">
<!--
.S_any { position:absolute; top:50px; width:100px; text-align:left; }
#S1    { left:10px;  }
#S2    { left:150px; }
select { width:120px; }
-->
</style>
</head>

<body>

<div id="S1" class="S_any">
  <select id="select_1" onChange="process(this,2)">
  <option value="0">Select here ....</option>
  <option value="NSW">NSW</option>
  <option value="Victoria">Victoria</option>
  <option value="Queensland">Queensland</option>
  </select>
</div>
<!-- end S1 -->
<div id="S2" class="S_any">
  <select id="select_2" onChange="finish()">
  <option value="0">Select here ....</option>
  </select></div>
<!- end S2>

</body>

</html>
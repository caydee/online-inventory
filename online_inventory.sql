-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2014 at 11:16 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `online_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee_details`
--

CREATE TABLE IF NOT EXISTS `employee_details` (
  `emp_no` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(23) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `id_no` int(11) NOT NULL,
  `date_of_employment` date NOT NULL,
  `telephone` varchar(35) NOT NULL,
  `email` varchar(34) NOT NULL,
  `mailing_address` varchar(67) NOT NULL,
  `location` varchar(60) NOT NULL,
  PRIMARY KEY (`emp_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=116 ;

--
-- Dumping data for table `employee_details`
--

INSERT INTO `employee_details` (`emp_no`, `firstname`, `lastname`, `surname`, `id_no`, `date_of_employment`, `telephone`, `email`, `mailing_address`, `location`) VALUES
(111, 'william', 'kibaara', 'maina', 6353322, '2014-07-06', '254-713-897832', 'trstywq@jhdsjs.com', '1442-00100 Nairobi', 'Nairobi'),
(112, 'ng''ang''a', 'ng''ang''a', 'ng''ang''a', 32324421, '2014-07-06', '253-733-443445', 'denkip207@yahoo.com', '1667-00300', 'Eldoret'),
(113, 'Albert', 'Heinstein', 'mwangi', 23322777, '2014-07-13', '254-784-987788', 'gysgdsgs@dbsj.ufd', '12222-00100', 'Nairobi'),
(114, 'Timoth', 'Lusala', 'masengo', 23211322, '2014-07-14', '254-878-888888', 'gysgkifdd@dbsj.ufd', '7737-01000', 'Lodwar'),
(115, 'antony', 'k', 'gzxbz', 8727234, '2014-08-20', '254-755-848443', 'gugkuygk@yahoo.com', '2763666', 'nairobi');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `stock_id` int(11) NOT NULL,
  `quantity-sold` int(11) NOT NULL,
  `date_out` datetime NOT NULL,
  `attendant` varchar(39) NOT NULL,
  KEY `stock_id` (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`stock_id`, `quantity-sold`, `date_out`, `attendant`) VALUES
(19, 20, '2014-07-31 17:39:11', '114'),
(20, 34, '2014-07-31 17:39:11', '114'),
(20, 200, '2014-08-20 11:37:32', '114'),
(20, 20, '2014-08-20 11:37:32', '114');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_name` varchar(30) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `brand` varchar(20) NOT NULL,
  `category` varchar(30) NOT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stock_id`, `stock_name`, `quantity`, `unit_price`, `brand`, `category`) VALUES
(19, 'cement', 480, 550, 'blue triangle', '50kg'),
(20, 'cement', 246, 1000, 'rhino', '50kg');

-- --------------------------------------------------------

--
-- Table structure for table `stock_details`
--

CREATE TABLE IF NOT EXISTS `stock_details` (
  `stock_id` int(11) NOT NULL,
  `date_in` datetime NOT NULL,
  `quantity` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`trans_id`),
  KEY `stock_id` (`stock_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `stock_details`
--

INSERT INTO `stock_details` (`stock_id`, `date_in`, `quantity`, `emp_id`, `trans_id`) VALUES
(19, '2014-07-31 17:37:55', 500, 113, 18),
(20, '2014-07-31 17:38:19', 500, 113, 19);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `emp_no` int(15) NOT NULL,
  `username` varchar(11) NOT NULL,
  `password` varchar(32) NOT NULL,
  `usertype` varchar(23) NOT NULL,
  `status` varchar(23) NOT NULL,
  KEY `emp_no` (`emp_no`),
  KEY `emp_no_2` (`emp_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`emp_no`, `username`, `password`, `usertype`, `status`) VALUES
(111, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'admin', 'active'),
(112, 'ng''ang''a', '95b9455163d99f3b8fadd14bee1afc7b', 'admin', 'active'),
(113, 'clerk', '34776981fa47aa6cf3f2915d11bae051', 'clerk', 'active'),
(114, 'accountant', '56f97f482ef25e2f440df4a424e2ab1e', 'accountant', 'active'),
(115, 'manager', '1d0258c2440a8d19e716292b231e3190', 'manager', 'active');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`stock_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stock_details`
--
ALTER TABLE `stock_details`
  ADD CONSTRAINT `stock_details_ibfk_1` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`stock_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_details_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `employee_details` (`emp_no`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`emp_no`) REFERENCES `employee_details` (`emp_no`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
